export const serveUrl: string = "http://192.168.43.41:3000/";
import {getLogger} from './index'

export class Request {
    constructor(url) {
        this.log = getLogger("=============>Request :");
        this.requestCanceled = false;
        this.url = url;
        this.response = {
            ok: undefined,
            status: undefined,
            headers: undefined,
            issue: undefined
        };
        this.timeout = 1500;
    }

    cancel() {
        this.requestCanceled = true;
    }

    get(headers): Promise {
        this.requestCanceled = false;
        const url = this.url;
        return new Promise((resolve, reject) =>
            this.request(url, this.createHeaders('GET', headers), resolve, reject));
    }

    post(headers, body): Promise {
        this.requestCanceled = false;
        const url = this.url;
        return new Promise((resolve, reject) =>
            this.request(url, this.createHeaders('POST', headers, body), resolve, reject));
    }

    put(headers, body): Promise {
        this.requestCanceled = false;
        const url = this.url;
        return new Promise((resolve, reject) =>
            this.request(url, this.createHeaders('PUT', headers, body), resolve, reject));
    }

    delete(headers): Promise {
        this.requestCanceled = false;
        const url = this.url;
        return new Promise((resolve, reject) =>
            this.request(url, this.createHeaders('DELETE', headers), resolve, reject));
    }

    createHeaders(method, headers = {}, body) {
        let createdHeaders = new Headers({...headers});
        body = body ? JSON.stringify(body) : body;
        this.log(JSON.stringify(createdHeaders));
        this.log(body ? body : 'No body ');
        createdHeaders.append('Accept', 'application/json');
        createdHeaders.append('Content-Type', 'application/json');
        return {
            method: method,
            headers: createdHeaders,
            body: body
        }
    }

    async request(url, headers, resolve, reject) {
        try {
            this.log(`${headers.method}  : ${url}`);
            const timeoutRequest = setTimeout(() => {
                this.response.ok = false;
                this.response.status = 599;
                this.response.issue = 'Connection timeout';
                reject(this.response)
            }, this.timeout);
            let response = await fetch(url, headers).then(res => {
                clearInterval(timeoutRequest);
                this.response.ok = res.ok;
                this.response.status = res.status;
                this.response.headers = res.headers;
                return res.json();
            });
            if (!this.requestCanceled) {
                if (this.response.ok) {
                    this.log(`Get succeeded `);
                    this.response.json = response;
                    resolve(this.response);
                } else {
                    this.log(`Get failed`);
                    this.response.issue = this.issueToText(response.issue);
                    reject(this.response)
                }
            } else {
                this.log(`Request canceled`);
            }
        } catch (err) {
            reject(err);
        }
    }

    issueToText(issue) {
        if (issue) {
            return issue.map(i => Object.getOwnPropertyNames(i).map(p => [p, i[p]].join(': '))).join('\n');
        }
        return undefined;
    }
}

export function getHeaderFromConnection(headers, HEADER = 'Last-Modified') {
    return JSON.stringify(headers.get("connection")
        .split('\n'))
        .split('","')
        .filter(ls => ls.indexOf(':') !== -1 && ls.split(':')[0] === HEADER)
        .map(ls => ls.slice(ls.indexOf(" "), ls.length))[0];
}
