import {Alert} from 'react-native'

export const registerRightAction = (navigator, action) => {
    let routes = navigator.getCurrentRoutes();
    if (routes.length > 0) {
        routes[routes.length - 1].rightAction = action;
    }
};

export const showError = (title, issue, callback) => {
    Alert.alert(
        title,
        issue,
        [{text: 'ok', onPress: () => callback()}]
    )
};

export class Listener {
    constructor(url, onMessage, onReconnect, onError) {
        this.url = url;
        this.ws = undefined;
        this.reconnect = undefined;
        this.onMessage = onMessage;
        this.onError = onError;
        this.onReconnect = onReconnect;
    }


    start() {
        this.ws = new WebSocket(this.url);
        const log = (message) => console.log(' WEB SOCKET :  ', message);

        this.ws.onopen = () => {
            log('onopen');
            if (this.reconnect) {
                this.reconnect = undefined;
                this.onReconnect && this.onReconnect();
            }
        };

        this.ws.onmessage = (e) => {
            log('onmessage');
            log(JSON.stringify(e.data));
            this.onMessage && this.onMessage(JSON.parse(e.data));
        };

        this.ws.onerror = (e) => {
            log('onerror');
        };

        this.ws.onclose = (e) => {
            log('onclose');
            log(e.code);
            log(e.reason);
            if (e.code !== 1000) {
                this.onError && this.onError();
                this.tryToReconnect();
            }
        };
    }

    tryToReconnect() {
        this.reconnect = true;
        const log = (message) => console.log(' WEB SOCKET :  ', message);
        log('ATTEMPTING TO RECONNECT ');
        this.start();
    }

    stop() {
        if (this.ws !== undefined) {
            this.ws.close();
        }
    }
}