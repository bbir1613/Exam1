export const getLogger = (TAG: string) => (message: string) => console.log(`${TAG} - ${message}`);
export {registerRightAction} from './util'
export  {showError} from './util'

export {serveUrl, headers, request} from './api'