import React, {Component} from "react";
import {StyleSheet, Text, TextInput, View, ActivityIndicator} from "react-native";
import styles from "../util/styles";
import {getLogger} from "../util";
import {clearIssue, addEvents} from "./Service";
import {showError, registerRightAction} from "../util/util";

const log = getLogger('EventAdd ');

const LIST_ROUTE = "api/add";

const initialState = {text: '', isLoading: false, issue: undefined, internetConnection: true};
export class EventAdd extends Component {
    constructor(props) {
        super(props);
        log('constructor()');
        this.state = initialState;
        this.store = this.props.store;
        this.currentRequest = undefined;
        registerRightAction(this.props.navigator, this.onSaveEvent.bind(this));
    }

    static get routeName() {
        return LIST_ROUTE;
    }

    static get route() {
        return {name: LIST_ROUTE, title: "Add event", rightText: "Save"}
    }

    componentWillMount() {
        log("ComponentWillMount()");
        this.unsubscribe = this.store.subscribe(() => this.storeToState(this.store.getState().event));
    }

    storeToState(eventState) {
        log('storeToState');
        const newState = Object.assign({}, this.state, {
            isLoading: eventState.isLoading,
            internetConnection: eventState.internetConnection,
            issue: eventState.issue
        });
        this.setState(newState, () => {
            log(`state updated ${JSON.stringify(this.state)} from store`)
        });
    }

    render() {
        log(`render`);
        let issue = this.state.issue ? this.state.issue : undefined;

        return (
            <View style={styles.content}>
                <ActivityIndicator animating={this.state.isLoading} style={styles.activityIndicator} size="large"/>
                {issue && showError('Error occurred', issue, clearIssue)}

                <TextInput value={this.state.text} onChangeText={(text) => this.setState({...this.state,text:text})}/>
            </View>
        )
    };

    componentDidMount() {
        this._isMounted = true;
        log("ComponentDidMount()");
    }

    componentWillUnmount() {
        this._isMounted = false;
        log("ComponentWillUnmount()");
        this.unsubscribe();
        this.setState({...initialState}, () => {
            log(`state cleared ${JSON.stringify(this.state)}`)
        });
    }

    onSaveEvent() {
        if (this.state.internetConnection) {
            this.store.dispatch(addEvents({text: this.state.text})).then(res => {
                if (!this.state.issue) this.props.navigator.pop();
                else log(`Can't add cuz there is an issue ${this.state.issue}`)
            })
        } else {
            showError('No internet connection', 'Make sure you\'re connected to internet', () => {
            });
        }
    }
}
