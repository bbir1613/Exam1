import React, {Component} from 'react';
import {StyleSheet, Text, View, ActivityIndicator, ListView} from 'react-native'
import {EventView} from './EventView'
import styles from '../util/styles'

import {getLogger} from '../util'
import {loadEvents, clearIssue, listenOnEvents} from "./Service";
import {showError, registerRightAction} from "../util/util";
import {EventAdd} from "./EventAdd";

const log = getLogger('List');

const LIST_ROUTE = "api/list";

const initialState = {items: [], isLoading: false, issue: undefined, dataSource: undefined, internetConnection: true};
export class List extends Component {
    constructor(props) {
        super(props);
        log('constructor()');
        this.state = initialState;
        this.store = this.props.store;
        this.ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        this.currentRequest = undefined;
        registerRightAction(this.props.navigator, this.onNewEvent.bind(this));
    }

    static get routeName() {
        return LIST_ROUTE;
    }

    static get route() {
        return {name: LIST_ROUTE, title: "Even list", rightText: "Add event"}
    }

    componentWillMount() {
        log("ComponentWillMount()");
        this.unsubscribe = this.store.subscribe(() => this.storeToState(this.store.getState().event));
        this.listener = listenOnEvents(this.store);
    }

    storeToState(eventState) {
        log('storeToState');
        const newState = Object.assign({}, this.state, {
            items: eventState.items,
            dataSource: this.ds.cloneWithRows(eventState.items),
            isLoading: eventState.isLoading,
            internetConnection: eventState.internetConnection,
            issue: eventState.issue
        });
        this.setState(newState, () => {
            log(`state updated ${JSON.stringify(this.state)} from store`)
        });
    }

    render() {
        log(`render`);
        let events = this.state.items;
        let issue = this.state.issue ? this.state.issue : undefined;
        log(`render events size ${events.length}`);

        return (
            <View style={styles.content}>
                <ActivityIndicator animating={this.state.isLoading} style={styles.activityIndicator} size="large"/>
                {issue && showError('Error occurred', issue, clearIssue)}
                {events.length > 0 &&
                <ListView
                    dataSource={this.state.dataSource}
                    enableEmptySections={true}
                    renderRow={event => <EventView event={event} onPress={(event) => log(event)}/>}
                />
                }
            </View>
        )
    };

    componentDidMount() {
        this._isMounted = true;
        log("ComponentDidMount()");
        this.listener && this.listener.start();
        this.store.dispatch(loadEvents()).then(request => this.setRequestField(request));
    }

    setRequestField(request: Request) {
        if (this._isMounted) {
            this.currentRequest = request
        } else {
            request.cancel();
        }
    }

    componentWillUnmount() {
        this._isMounted = false;
        log("ComponentWillUnmount()");
        this.unsubscribe();
        this.setState({...initialState}, () => {
            log(`state cleared ${JSON.stringify(this.state)}`)
        });
        this.listener && this.listener.stop();
        this.currentRequest && this.currentRequest.cancel();
    }


    onNewEvent() {
        if (this.state.internetConnection) {
            this.props.navigator.push({...EventAdd.route});
        } else {
            showError('No internet connection', 'Make sure you\'re connected to internet', () => {
            });
        }
    }
}
