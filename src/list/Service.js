import {getLogger} from '../util'
const log = getLogger("EventReducer");
import {AsyncStorage} from 'react-native'

import {serveUrl, headers, request}from '../util'
import {getHeaderFromConnection, createHeaders, Request} from "../util/api";
import {Listener} from "../util/util";

const ITEMS = 'items';

//apis
export const listenOnEvents = (store): Listener => {
    return new Listener(serveUrl + 'event', (event) => {
        store.dispatch(addEventToState(event));
    }, () => {
        store.dispatch(internetConnection());
    }, () => {
        store.dispatch(noInternet());
    });
};

const addEventToState = (event) => async(dispatch, getState) => {
    let items = [].concat(getState().event.items);
    items.push(event);
    dispatch(loadEventsSucceeded(items));
    AsyncStorage.setItem(ITEMS, JSON.stringify(items));
};

const noInternetConnection = () => async(dispatch, getState) => {
    dispatch(noInternet());
};

const internetConnection = () => async(dispatch, getState) => {
    dispatch(internet());
};


export const addEvents = (event) => async(dispatch, getState) => {
    const request = new Request(serveUrl + 'event');
    request.post({}, event).then(async res => {
    }).catch(async error => {
        log('catch error');
        if (error.status === 599) {
            log('no connection to internet');
            dispatch(noInternet());
        } else {
            console.log(error.issue);
            dispatch(loadEventsIssue(error.issue))
        }
    });
};

export const loadEvents = () => async(dispatch, getState) => {
    log(`loadEvents...`);
    dispatch(loadEventsStarted());

    const dispatchOnSuccess = (items) => {
        dispatch(loadEventsSucceeded(items));
        dispatch(loadEventsFinished());
    };

    const request = new Request(serveUrl + 'event');
    request.get().then(async res => {
        if (res.status === 200) {
            log('is modified');
            AsyncStorage.setItem('Last-Modified', getHeaderFromConnection(res.headers));
            AsyncStorage.setItem(ITEMS, JSON.stringify(res.json));
            dispatchOnSuccess(res.json);
            log(JSON.stringify(res.json));
        }
        if (res.status === 304) {
            log('Not modified');
            const items = await AsyncStorage.getItem(ITEMS);
            dispatchOnSuccess(JSON.parse(items));
        }
    }).catch(async error => {
        log('catch error');
        if (error.status === 599) {
            log('no connection to internet');
            dispatch(noInternet());
            const items = await AsyncStorage.getItem(ITEMS);
            dispatchOnSuccess(JSON.parse(items));
        } else {
            console.log(error.issue);
            dispatch(loadEventsIssue(error.issue))
        }
    });

    return request;
};

export const clearIssue = () => async(dispatch, getState) => {
    log(`clearIssue ...`);
    dispatch(clearEventsIssue());
};
//ACTIONS
const loadEventsStarted = () => {
    return {type: LOAD_EVENTS_STARTED};
};
const loadEventsSucceeded = (items) => {
    return {type: LOAD_EVENTS_SUCCEEDED, items: items}
};
const loadEventsFinished = () => {
    return {type: LOAD_EVENTS_FINISHED};
};
const loadEventsIssue = (issue: string) => {
    return {type: LOAD_EVENTS_ISSUE, issue: issue ? issue : "error occurred"};
};
const clearEventsIssue = (issue) => {
    return {type: CLEAR_EVENTS_ISSUE, issue: issue};
};

const noInternet = () => {
    return {type: NO_INTERNET_CONNECTION};
};

const internet = () => {
    return {type: INTERNET_CONNECTION};
};

//HANDLE ACTIONS
handleActions = {};

const LOAD_EVENTS_STARTED = "LOAD_EVENTS_STARTED";
handleActions[LOAD_EVENTS_STARTED] = (state, action) => {
    return {...state, isLoading: true}
};

const LOAD_EVENTS_SUCCEEDED = "LOAD_EVENTS_SUCCEEDED";
handleActions[LOAD_EVENTS_SUCCEEDED] = (state, action) => {
    return Object.assign({}, state, {items: action.items})
};

const LOAD_EVENTS_FINISHED = "LOAD_EVENTS_FINISHED";
handleActions[LOAD_EVENTS_FINISHED] = (state, action) => {
    return Object.assign({}, state, {isLoading: false})
};

const LOAD_EVENTS_ISSUE = "LOAD_EVENTS_ISSUE";
handleActions[LOAD_EVENTS_ISSUE] = (state, action) => {
    return {...state, issue: action.issue, isLoading: false}
};

const CLEAR_EVENTS_ISSUE = "CLEAR_EVENTS_ISSUE";
handleActions[CLEAR_EVENTS_ISSUE] = (state, action) => {
    return {...state, issue: undefined, isLoading: false}
};

const NO_INTERNET_CONNECTION = "NO_INTERNET_CONNECTION";
handleActions[NO_INTERNET_CONNECTION] = (state, action) => {
    return {...state, internetConnection: false}
};

const INTERNET_CONNECTION = "INTERNET_CONNECTION";
handleActions[INTERNET_CONNECTION] = (state, action) => {
    return {...state, internetConnection: true}
};


//----------------------reducer
const initialState = {items: [], isLoading: false, issue: undefined, internetConnection: true};
export const eventReducer = (state = initialState, action) => {
    log('eventReducer');
    return handleActions[action.type] ? handleActions[action.type](state, action) : state;
};