import React, {Component} from 'react';
import {AppRegistry} from 'react-native';
import {createStore, applyMiddleware, combineReducers} from 'redux';
import createLogger from 'redux-logger';
import thunk from 'redux-thunk';

import {Router} from './src/Router'

import {eventReducer} from './src/list/Service'

const rootReducer = combineReducers({event: eventReducer});
const store = createStore(rootReducer, applyMiddleware(thunk, createLogger({colors: {}})));

export default class Exam1 extends Component {
    render() {
        return <Router store={store}/>
    }
}
AppRegistry.registerComponent('Exam1', () => Exam1);
